<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * This model defines Wine entity table and 
 * its relationships with other entities
*/
class Wine extends Model
{
    protected $table = 'wines';
    protected $fillable = ['name','availability_date'];

    //It can belong to many orders
    public function orders(){
        return $this->belongsToMany('App\Order');
    }
}
