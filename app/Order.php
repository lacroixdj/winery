<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * This model defines Order entity table and 
 * its relationships with other entities
*/
class Order extends Model
{
    protected $table = 'orders';

    //It can be related with many wines
    public function wines(){
        return $this->belongsToMany('App\Wine');
    }
}
