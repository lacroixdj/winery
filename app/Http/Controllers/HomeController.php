<?php

//Application namespace
namespace App\Http\Controllers;

//Request library helper
use Illuminate\Http\Request;

//Wine model
use \App\Wine;

//Order model
use \App\Order;

/**
 * This controller class handles  application home, 
 * and main features.
 *
 * @return \Illuminate\Contracts\Support\Renderable
 */
class HomeController extends Controller
{
    
    /**
     * This method shows the application home, 
     * gets the rss feed and display available wines
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){

        //Wines items array
        $wines = [];

        //Getting RSS feed
        $feed = \Feeds::make('https://www.winespectator.com/rss/rss?t=dwp');

        //Grab all items inside the RSS xml
        $items = $feed->get_items();

            //Today date and unixtime
            $today = date('Y-m-d');
            $today_time = strtotime($today);

            //Looping the items
            foreach($items as $item){

                //Getting item attributes
                $title  = $item->get_title();                
                $w_date = $item->get_date();
                
                //Parsing and formatting the date using  Carbon
                $c_date = \Carbon\Carbon::parse($w_date);
                $iso_date = $c_date->format('Y-m-d');
               
                //Preparing the wine item for database table
                $wine_item = array(
                    "name"=>$title, 
                    "availability_date"=> $iso_date,
                );  
                
                //Update or create is a massive upsert
                $wine = \App\Wine::updateOrCreate(
                    ['name' => $wine_item['name']],
                    ['availability_date' => $wine_item['availability_date']]
                );
            }//End foreach
        
        //Fetching wines table records from DB
        $wines_from_db = \App\Wine::all()->sortByDesc('availability_date');

        //Preparing view data    
        $view_data = [
            "wines" => $wines_from_db,
            "today_date" => $today,
            "today_time" => $today_time,
        ];
        
        //Retunrning home view with data
        return view('home', $view_data);
    }


    /**
     * This methd saves the wines order to the database
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveOrder(Request $request){

        //Request imput onbject
        $input = $request->input();

        //Date variables
        $today = date('Y-m-d');
        $today_time = strtotime($today);

        //Validation wine selection can't be empty
        if(empty($input["wine_checkbox"])) {            
            $request->session()->flash('error_msg', 'Error! - you must select one wine at least');
        } else {
            
            //Creating the order first
            $order = new \App\Order;
            
            //Persisting order to database
            $saved = $order->save();
            
            //Attaching wine list to the order  (n:m) table 
            $attached = $order->wines()->attach($input["wine_checkbox"]);

            //Checking the result
            if($saved){
                $request->session()->flash('success_msg', "Success! - Wine order was issued - Order No:{$order->id}");
            } else{
                $request->session()->flash('error_msg', 'Error! - Something wents wrong during order creation');
            }

        }

        //Listing the wines from database
        $wines_from_db = \App\Wine::all()->sortByDesc('availability_date');
        
        //View data
        $view_data = [
            "wines" => $wines_from_db,
            "today_date" => $today,
            "today_time" => $today_time
        ];
        
        //Returning the original view 
        return view('home', $view_data);
    }


    /**
     * This method shows a orders list from database
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showOrders(){

        //Fetching the orders from DB
        $orders_from_db = \App\Order::all()->sortByDesc('updated_at');
        
        //Assigning data
        $view_data = [
            "orders" => $orders_from_db,
        ];

        //Returning view
        return view('orders', $view_data);
    }

    
}
