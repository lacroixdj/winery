# Trivago Winery

This README.md file shows step by step how to install and run the Trivago Winery web application.

## Stack:

- PHP >=7.1.3
- SQLite3
- HTML, JS, CSS.
- Laravel  (framework version 5.8.2)
- Boostrap (included in Laravel)
- JQuery   (included in Laravel)


## Requirements:

Server requirements in order to get the application running. 
**Note:** Most of the following requirements are Laravel framework requirements.

- PHP >=7.1.3
- SQLite3
- Composer
- PHP sqlite3 module
- PHP pdo_sqlite module
- Mbstring PHP Extension
- XML PHP Extension
- Tokenizer PHP Extension
- JSON PHP Extension

## Cloning the repository from Bitbucket:

```
# cd /path/to/your/web/document/root 
# git clone https://lacroixdj@bitbucket.org/lacroixdj/winery.git
# cd winery/
```

## Setup:

First we need to install Laravel and packages using composer:

```
# composer install 
```

For practical reasons we are using SQLite, so the database is  embeded inside the code  **winery/database/database.sqlite**. So make sure to assign write permissions to that file.

Once the app is installed we proceed to run migrations in order to create DB Schema and tables. 

```
# php artisan migrate 
```

## Running the server:

Finally we need to start the embeded web server that comes out of the box with Laravel. 

```
# php artisan serve 
```

If everything wents fine we should see an ouput like this:

```
# Laravel development server started: <http://127.0.0.1:8000> 
```

## Browsing the app:

Open your web browser and go to  http://127.0.0.1:8000 ; you should see the app running.


## Source code structure:

We don't pretend to explain all the Laravel folders structure, however we are going to highlight the most relevants directories used in this app.


- **app/** The app directory contains the core code of the application. Application **Controllers** and  **Models** classes are located in this directory.
    * **Wine.php**  Wine entity model.
    * **Order.php** Order entity model.
    * **Http/Controllers/HomeController.php** This controller handles application main features, application logic methods are defined here.


- **config/** The config directory, as the name implies, contains all application's configuration files.
    * **app.php** Holds the app man configuration variables, in this case is used with default values.
    * **database.php** In this file we specify default database connection settings.


- **database/** The database directory contains database migrations, also this directory to holds the SQLite database.
    * **database.sqlite** Embeded SQLite application database.
    * **Migrations/** Migrations are like version control for database, allowing to easily modify and share the application's database schema. In few words this directory contains database tables schema definitions.


- **public/** The public directory contains the index.php file, which is the entry point for all requests entering the application. This directory also houses public assets such as images, JavaScript, and CSS.
    * **js/home.js** Custom javascript file where are located some validation and helper functions.
    * **css/home.css** Custom CSS file with custom styles.


- **resources/** The resources directory contains the views as well raw, un-compiled assets such as LESS, SASS, or JavaScript.
    * **views/home.blade.php** Application main view, display the wine list.
    * **views/orders.blade.php** Display a list of issued wine orders.


- **routes/** The routes directory contains all of the route definitions in the application. 
     * **web.php** This file defines routes that are for your web interface. These routes are assigned the web middleware group, which provides features like session state and CSRF protection.

- **tests/** The tests directory contains automated tests. Each test class is suffixed with the word Test. You may run  application tests using the phpunit or php vendor/bin/phpunit commands.
     * **unit/WineryTest.php** This file contains PHP Unit tests for application methods.


## Running the tests:

```
# php vendor/bin/phpunit 
```