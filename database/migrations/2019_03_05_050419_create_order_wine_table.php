<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


/**
 * This migration defines many to many relation table  
 * between wines and orders
*/
class CreateOrderWineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_wine', function (Blueprint $table) {
            
            //Id serial
            $table->bigIncrements('id');
            
            //Control timestamp
            $table->timestamps();

            //Order id from orders table
            $table->integer('order_id')->unsigned();
            
            //Wine ide from wines table
            $table->integer('wine_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_wine');
    }
}
