<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


/**
 * This migration defines wines table structure
 * 
*/
class CreateWinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wines', function (Blueprint $table) {
            
            //Id serial
            $table->bigIncrements('id');
            
            //Control timestamp
            $table->timestamps();
            
            //Wine's name
            $table->string('name');
            
            //Date of availability
            $table->dateTime('availability_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wines');
    }
}
