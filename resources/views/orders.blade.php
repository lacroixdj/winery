@extends('layouts.app')

@section('content')
<div class="container">     
    <div class="row justify-content-center my-4">
        <div class="col-md-12">
                        
                <div class="card">
                    <div class="card-header">
                        Orders list:
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="accordion">
                                <thead>
                                    <tr>
                                        <th>id:</th>
                                        <th>Date:</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($orders as $order)
                                        <tr class="order_record">
                                            <td>
                                                {{ $order->id }}
                                            </td>
                                            <td>
                                                {{ $order->created_at }}
                                            </td>
                                        </tr>

                                        <tr class="order_record" id="collapse-{{ $order->id }}" class="collapse" colspan="2">
                                            <td>
                                                <div class="card-body">
                                                    <ul>
                                                        @foreach ($order->wines as $wine)
                                                            <li>{{ $wine->name }} </li>
                                                        @endforeach
                                                    </ul>    
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>                    
                    </div>
                </div>
            
        </div>
    </div>
</div>
@endsection
