@extends('layouts.app')

@section('content')
<div class="container">
     <div class="row justify-content-center my-4" >
        <div class="col-md-12" >
            <div class="card">
                <div class="card-header">Update wines menu:</div>
                <div class="card-body"> 
                    
                    <form class="form-inline" action="/home">
                          <div class="form-group col-10 mx-sm-3 mb-2">
                            <label for="rss_url" class="sr-only">RSS Feed</label>
                            <input type="text"  style="width:100%" class="form-control" id="rss_url" value="https://www.winespectator.com/rss/rss?t=dwp" readonly> 
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Update</button>
                    </form>      
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center my-4">
        <div class="col-md-12">
            <form action="/order/save" method="POST"> 
                @csrf
                <div class="card">
                    <div class="card-header">
                        Wines Menu for today  {{ $today_date }}:
                        <button type="submit" class="btn btn-primary mb-2 float-right">Process Order</button>
                    </div>
                    @if (session('error_msg'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error_msg') }}
                        </div>
                    @elseif (session('success_msg'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success_msg') }}
                        </div>
                    @endif
                    
                    <div class="card-body">
                        
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>id:</th>
                                            <th>Wine:</th>
                                            <th>Date:</th>
                                            <th>Availability:</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($wines as $wine)
                                            @php
                                                $is_available = (int)(strtotime($wine->availability_date) ==  $today_time);
                                            @endphp
                                            <tr class="row_record" data-available="{{$is_available}}">
                                                <td><input type="checkbox" name="wine_checkbox[]" value="{{ $wine->id }}" @if(!$is_available) disabled @endif  data-available="{{$is_available}}"></td>
                                                <td>{{ $wine->id }}</td>
                                                <td>{{ $wine->name }}</td>
                                                <td>{{ $wine->availability_date }}</td>
                                                @if ($is_available)
                                                    <td style="color:green">Available</td>    
                                                @else
                                                    <td style="color:red">Not Available</td>
                                                @endif                                        
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                    
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
