/**
 * Javascript validation and helper functions
 * 
*/


//DOM loaded and rendered? Ok let's go
$(document).ready(function () {
    
    //Event listener for observing clicks on wines rows
    //If wine is not available throws an error alert
    $(".row_record").click(function () {
        var is_available = $(this).attr('data-available');
        is_available *= 1;
        if (empty(is_available)) alert("Sorry the selected wine is not currently available");        
    });

    //Event listener for observing clicks on wines list checkboxes
    //If wine is not available throws an error alert and uncheck the checkbox
    $("[type=checkbox]").click(function () {
        var is_available = $(this).attr('data-available');
        is_available *= 1;
        if (empty(is_available)) {
            $(this).checked = false;
            $(this).disabled = true;
            alert("Sorry the selected wine is not currently available");
            return false;
        }
        return true;
    });
});


/**
 *  This function emulates PHP empty() function 
 *  Is very useful for checking variables
 *  Author: jesus.farias@gmail.com
*/
function empty(val) {
    try {
        if (val == null) return true;
        switch (typeof (val)) {
            case 'number':
                return (val == 0);

            case 'boolean':
                return (val == false);

            case 'string':
                if (val == "" || val.match(/^\s*$/) || val=="0") return true;
                else return false;

            case 'object':
                return (val.length == 0);

            case 'function':
                return false;

            case 'undefined':
                return true;
        }
    } catch (e) {
        console.log(e);
        return true;
    }
}

