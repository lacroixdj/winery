<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* 
    Route::get('/', function () {
        return view('welcome');
    });
*/

// Auth, user routes (not used)
Auth::routes();

//Wine list routes
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//Orders routes
Route::post('/order/save', 'HomeController@saveOrder')->name('saveOrder');
Route::get('/order/list', 'HomeController@showOrders')->name('showOrders');





